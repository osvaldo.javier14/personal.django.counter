from django.http import JsonResponse


def health(request):
    return JsonResponse({
        'ok': True
    })


def root(request):
    return JsonResponse({
        'message': 'Hello world'
    })
