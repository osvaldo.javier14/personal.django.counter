#!/bin/bash
set -e

set -o errexit
set -o pipefail
set -o nounset

mkdir -p /src/app/logs

# prepare log files and start outputting logs to stdout
touch /src/app/gunicorn.log
touch /src/app/access.log
tail -n 0 -f /src/app/*.log &

python manage.py makemigrations
python manage.py migrate
# python manage.py collectstatic --noinput

if [ "$ENV" = "dev" ] ; then
    echo Running Development Server
    python manage.py runserver 0:8000
else
    echo Starting Gunicorn.
    exec gunicorn pocloudflare.wsgi \
        --name pocloudflareName \
        --bind 0.0.0.0:8000 \
        --workers 3 \
        --timeout 120 \
        --worker-class gevent \
        --log-level=info \
        --log-file=/src/app/gunicorn.log \
        --access-logfile=/src/app/access.log
fi
