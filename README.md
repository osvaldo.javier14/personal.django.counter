
## Hola 🔥
Este repo es un POC usando el DNS de Cloudflare y Route 53.

## Tech Stack 📝

**Client:**
 - Django Templates

**Server:**
 - Django

**DevOps:**
 - Docker
 - Docker compose
 - Gitlab-ci
 - AWS-ECS
 - AWS-ECR
 - AWS-EC2-Load Balancer

### ToDo
 - hacer un overwrite de los archivos docker (dev-prod)