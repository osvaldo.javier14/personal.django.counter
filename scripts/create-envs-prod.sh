cp .env.example .env

# reemplazar las variables de entorno en el archivo .env
sed -i 's|<DEBUG>|'"$DEBUG"'|g' .env
sed -i 's|<ENV>|prod|g' .env
sed -i 's|<SECRET_KEY>|'"$SECRET_KEY"'|g' .env
sed -i 's|<DOMAIN>|'"$DOMAIN_PROD"'|g' .env

sed -i 's|<DB_HOSTNAME>|'"$DB_HOSTNAME_PROD"'|g' .env
sed -i 's|<DB_NAME>|'"$DB_NAME_PROD"'|g' .env
sed -i 's|<DB_USER>|'"$DB_USER_PROD"'|g' .env
sed -i 's|<DB_PASSWORD>|'"$DB_PASSWORD_PROD"'|g' .env
sed -i 's|<DB_PORT>|'"$DB_PORT_PROD"'|g' .env

sed -i 's|<AWS_STORAGE_BUCKET_NAME>|'"$AWS_STORAGE_BUCKET_NAME"'|g' .env
