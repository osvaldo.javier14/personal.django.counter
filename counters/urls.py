from django.urls import path
from . import views


PREFIX_VERSION_API_1 = 'api/v1'

urlpatterns = [
    path('home', views.home),
    path(f'{PREFIX_VERSION_API_1}', views.counters),
]
