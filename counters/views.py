from django.http import JsonResponse
from .models import VisitCount
from django.shortcuts import render


VERSION = '1.0.3'


def counters(request):
    visit, created = VisitCount.objects.get_or_create(id=1)
    visit.count += 1
    visit.save()

    return JsonResponse({
        'count': visit.count,
        'version': VERSION
    })


def home(request):
    context = {'version': VERSION}
    return render(
        request,
        'counter_home.html',
        context=context
    )
